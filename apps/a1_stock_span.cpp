#include <iostream>
#include "stack.hpp"

int main(int argc, char **argv)
{
    Istack::IStack1000 st_span;
    int n = 0;
    std ::cout << "Enter the number of days : ";
    std ::cin >> n;
    int price[n - 1];
    for (int j = 0; j < n; j++)
    {
        std ::cout << "Enter the price of day" << j + 1 << " : ";
        std ::cin >> price[j];
    }
    std ::cout << "*****************************************" << std::endl;
    int span[n];
    int garbage = 0;
    span[0] = 1;
    Istack::push(st_span, 0);
    for (int i = 1; i < n; i++)
    {
        while (st_span.top != -1 && price[i] >= price[st_span.top])
        {
            garbage = Istack::pop(st_span);
        }
        if (st_span.top == -1)
        {
            span[i] = i + 1;
        }
        else
        {
            span[i] = i - st_span.top;
        }

        Istack::push(st_span, i);
    }
    for (int i = 0; i < n; i++)
    {
        std ::cout << "Span of day" << i + 1 << " = " << span[i] << std::endl;
    }
}
