#include "mathematics.hpp" // for mathematics::heron
#include <iostream>        // for std::cout
#include <algorithm>       // for std::atof
#include <string>

int main(int argc, char **argv)
{
    mathematics::Triangle t{0, 0, 0};
    t.a = std::atof(argv[1]); // a,b and c are the sides of the triangle
    t.b = std::atof(argv[2]);
    t.c = std::atof(argv[3]);
    double area = mathematics::heron(t);
    std::cout << "Area =  " << area << std::endl;
}
