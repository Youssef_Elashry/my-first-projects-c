#include "distance.hpp"
#include <iostream>
#include <algorithm>
int main(int argc, char **argv)
{

    distance p1, p2;
    p1.x = std::atoi(argv[1]);
    p1.y = std::atoi(argv[2]);
    p2.x = std::atoi(argv[3]);
    p2.y = std::atoi(argv[4]);
    std::cout << "Distance = " << distance1(p1, p2) << std::endl;
}
