#ifndef MEMBER3_HPP
#define MEMBER3_HPP
#include <iostream>
//namespace for integer linkedlist
namespace ILL
{
struct IntegerNode
{
    int data;
    IntegerNode *next = nullptr;
};
struct IntegersLL
{
    IntegerNode *front = nullptr;
};
void insertFront(IntegersLL &list, int data)
{
    IntegerNode *new_node = new IntegerNode;
    new_node->data = data;
    new_node->next = list.front;
    list.front = new_node;
}
void insertBack(IntegersLL &list, int data)
{
    IntegerNode *new_node = new IntegerNode;
    new_node->data = data;
    new_node->next = nullptr;
    if (list.front == nullptr)
    {
        list.front = new_node;
    }
    else
    {
        IntegerNode *temp = list.front;
        while (temp->next != nullptr)
        {
            temp = temp->next;
        }

        temp->next = new_node;
    }
}
void removeFront(IntegersLL &list)
{
    if (list.front != nullptr)
    {
        IntegerNode *current = list.front;
        list.front = list.front->next;
        delete current;
    }
}
void removeBack(IntegersLL &list)
{
    if (list.front != nullptr)
    {
        IntegerNode *current, *previous;
        if (list.front->next == nullptr)
        {
            current = list.front;
            list.front = nullptr;
            delete current;
        }
        else
        {
            current = list.front;
            while (current->next != nullptr)
            {
                previous = current;
                current = current->next;
            }
            previous->next = nullptr;
            delete current;
        }
    }
}
void removeAt(IntegersLL &list, int index)
{
    if ((list.front != nullptr) && (index > 0))
    {
        IntegerNode *current = list.front;
        IntegerNode *previous;
        for (int i = 1; i != index; i++)
        {
            previous = current;
            current = current->next;
        }
        previous->next = current->next;
        delete current;
    }
}
int front(IntegersLL &list)
{
    if (list.front != nullptr)
    {
        return list.front->data;
    }
}
int back(IntegersLL &list)
{
    if (list.front != nullptr)
    {
        IntegerNode *current = list.front;
        while (current->next != nullptr)
        {
            current = current->next;
        }
        return current->data;
    }
}
int getAt(IntegersLL &list, int index)
{
    if ((list.front != nullptr) && (index > 0))
    {
        IntegerNode *current = list.front;
        for (int i = 1; i != index; i++)
        {
            current = current->next;
        }
        return current->data;
    }
}
void filter(IntegersLL &list, int data)
{
    if (list.front != nullptr)
    {
        if (list.front->data == data)
        {
            IntegerNode *current1 = list.front;
            list.front = list.front->next;
            delete current1;
        }
        else
        {
            IntegerNode *current2 = list.front;
            IntegerNode *previous;
            while (current2->data != data)
            {
                previous = current2;
                current2 = current2->next;
            }
            previous->next = current2->next;
            delete current2;
        }
    }
}
bool isEmpty(IntegersLL &list)
{
    return (list.front == nullptr);
}
int size(IntegersLL &list)
{
    IntegerNode *current = list.front;
    int i = 0;
    while (current != nullptr)
    {
        current = current->next;
        ++i;
    }
    return i;
}
void printAll(IntegersLL &list)
{
    IntegerNode *current = list.front;
    while (current != nullptr)
    {
        std::cout << current->data << std::endl;
        current = current->next;
    }
}
void clear(IntegersLL &list)
{
    IntegerNode *current1 = list.front;
    IntegerNode *current2;
    while (current1 != nullptr)
    {
        current2 = current1->next;
        delete current1;
        current1 = current2;
    }
    list.front = nullptr;
}
}
//namespace for Double linkedlist
//...............................
//...............................
namespace DLL
{
struct DoubleNode
{
    double data;
    DoubleNode *next = nullptr;
};
struct DoublesLL
{
    DoubleNode *front = nullptr;
};
void insertFront(DoublesLL &list, double data)
{
    DoubleNode *new_node = new DoubleNode;
    new_node->data = data;
    new_node->next = list.front;
    list.front = new_node;
}
void insertBack(DoublesLL &list, double data)
{
    DoubleNode *new_node = new DoubleNode;
    new_node->data = data;
    new_node->next = nullptr;
    if (list.front == nullptr)
    {
        list.front = new_node;
    }
    else
    {
        DoubleNode *temp = list.front;
        while (temp->next != nullptr)
        {
            temp = temp->next;
        }

        temp->next = new_node;
    }
}
void removeFront(DoublesLL &list)
{
    if (list.front != nullptr)
    {
        DoubleNode *current = list.front;
        list.front = list.front->next;
        delete current;
    }
}
void removeBack(DoublesLL &list)
{
    if (list.front != nullptr)
    {
        DoubleNode *current, *previous;
        if (list.front->next == nullptr)
        {
            current = list.front;
            list.front = nullptr;
            delete current;
        }
        else
        {
            current = list.front;
            while (current->next != nullptr)
            {
                previous = current;
                current = current->next;
            }
            previous->next = nullptr;
            delete current;
        }
    }
}
void removeAt(DoublesLL &list, int index)
{
    if ((list.front != nullptr) && (index > 0))
    {
        DoubleNode *current = list.front;
        DoubleNode *previous;
        for (int i = 1; i != index; i++)
        {
            previous = current;
            current = current->next;
        }
        previous->next = current->next;
        delete current;
    }
}
double front(DoublesLL &list)
{
    if (list.front != nullptr)
    {
        return list.front->data;
    }
}
double back(DoublesLL &list)
{
    if (list.front != nullptr)
    {
        DoubleNode *current = list.front;
        while (current->next != nullptr)
        {
            current = current->next;
        }
        return current->data;
    }
}
double getAt(DoublesLL &list, int index)
{
    if ((list.front != nullptr) && (index > 0))
    {
        DoubleNode *current = list.front;
        for (int i = 1; i != index; i++)
        {
            current = current->next;
        }
        return current->data;
    }
}
void filter(DoublesLL &list, double data)
{
    if (list.front != nullptr)
    {
        if (list.front->data == data)
        {
            DoubleNode *current1 = list.front;
            list.front = list.front->next;
            delete current1;
        }
        else
        {
            DoubleNode *current2 = list.front;
            DoubleNode *previous;
            while (current2->data != data)
            {
                previous = current2;
                current2 = current2->next;
            }
            previous->next = current2->next;
            delete current2;
        }
    }
}
bool isEmpty(DoublesLL &list)
{
    return (list.front == nullptr);
}
int size(DoublesLL &list)
{
    DoubleNode *current = list.front;
    int i = 0;
    while (current != nullptr)
    {
        current = current->next;
        ++i;
    }
    return i;
}
void printAll(DoublesLL &list)
{
    DoubleNode *current = list.front;
    while (current != nullptr)
    {
        std::cout << current->data << std::endl;
        current = current->next;
    }
}
void clear(DoublesLL &list)
{
    DoubleNode *current1 = list.front;
    DoubleNode *current2;
    while (current1 != nullptr)
    {
        current2 = current1->next;
        delete current1;
        current1 = current2;
    }
    list.front = nullptr;
}
}
//namespace for Char linkedlist
//...............................
//...............................
namespace CLL
{
struct CharNode
{
    char data;
    CharNode *next = nullptr;
};
struct CharsLL
{
    CharNode *front = nullptr;
};
void insertFront(CharsLL &list, char data)
{
    CharNode *new_node = new CharNode;
    new_node->data = data;
    new_node->next = list.front;
    list.front = new_node;
}
void insertBack(CharsLL &list, char data)
{
    CharNode *new_node = new CharNode;
    new_node->data = data;
    new_node->next = nullptr;
    if (list.front == nullptr)
    {
        list.front = new_node;
    }
    else
    {
        CharNode *temp = list.front;
        while (temp->next != nullptr)
        {
            temp = temp->next;
        }

        temp->next = new_node;
    }
}
void removeFront(CharsLL &list)
{
    if (list.front != nullptr)
    {
        CharNode *current = list.front;
        list.front = list.front->next;
        delete current;
    }
}
void removeBack(CharsLL &list)
{
    if (list.front != nullptr)
    {
        CharNode *current, *previous;
        if (list.front->next == nullptr)
        {
            current = list.front;
            list.front = nullptr;
            delete current;
        }
        else
        {
            current = list.front;
            while (current->next != nullptr)
            {
                previous = current;
                current = current->next;
            }
            previous->next = nullptr;
            delete current;
        }
    }
}
void removeAt(CharsLL &list, int index)
{
    if ((list.front != nullptr) && (index > 0))
    {
        CharNode *current = list.front;
        CharNode *previous;
        for (int i = 1; i != index; i++)
        {
            previous = current;
            current = current->next;
        }
        previous->next = current->next;
        delete current;
    }
}
char front(CharsLL &list)
{
    if (list.front != nullptr)
    {
        return list.front->data;
    }
}
char back(CharsLL &list)
{
    if (list.front != nullptr)
    {
        CharNode *current = list.front;
        while (current->next != nullptr)
        {
            current = current->next;
        }
        return current->data;
    }
}
char getAt(CharsLL &list, int index)
{
    if ((list.front != nullptr) && (index > 0))
    {
        CharNode *current = list.front;
        for (int i = 1; i != index; i++)
        {
            current = current->next;
        }
        return current->data;
    }
}
void filter(CharsLL &list, char data)
{
    if (list.front != nullptr)
    {
        if (list.front->data == data)
        {
            CharNode *current1 = list.front;
            list.front = list.front->next;
            delete current1;
        }
        else
        {
            CharNode *current2 = list.front;
            CharNode *previous;
            while (current2->data != data)
            {
                previous = current2;
                current2 = current2->next;
            }
            previous->next = current2->next;
            delete current2;
        }
    }
}
bool isEmpty(CharsLL &list)
{
    return (list.front == nullptr);
}
int size(CharsLL &list)
{
    CharNode *current = list.front;
    int i = 0;
    while (current != nullptr)
    {
        current = current->next;
        ++i;
    }
    return i;
}
void printAll(CharsLL &list)
{
    CharNode *current = list.front;
    while (current != nullptr)
    {
        std::cout << current->data << std::endl;
        current = current->next;
    }
}
void clear(CharsLL &list)
{
    CharNode *current1 = list.front;
    CharNode *current2;
    while (current1 != nullptr)
    {
        current2 = current1->next;
        delete current1;
        current1 = current2;
    }
    list.front = nullptr;
}
}

#endif // MEMBER3_HPP
