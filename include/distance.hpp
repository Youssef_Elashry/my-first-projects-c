#include <cmath>
#include <iostream>
struct distance
{

    int x;
    int y;
};
double distance1(distance p1, distance p2)
{

    double d = std::sqrt(((p1.x - p2.x) * (p1.x - p2.x)) + ((p1.y - p2.y) * (p1.y - p2.y)));
    return d;
}