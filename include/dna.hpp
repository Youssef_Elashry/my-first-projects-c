#ifndef DNA_HPP
#define DNA_HPP
#include "arrays.hpp"
namespace dna
{
using DNAArray = arrays::CharacterArray;
struct Counters
{
  int counterA;
  int counterC;
  int counterG;
  int counterT;
};
char complementaryBase(char base)
{
  switch (base)
  {
  case 'A':
  {
    return 'T';
    break;
  }
  case 'T':
  {
    return 'A';
    break;
  }
  case 'C':
  {
    return 'G';
    break;
  }
  default:
  {
    return 'C';
  }
  }
}
char *complementarySequence(char *base, int size)
{

  char *complementary_Sequence = new char[size];
  int i, j;
  for (i = 0, j = size; (i < size && j > 0); i++, j--)
  {
    complementary_Sequence[i] = base[j];

    complementary_Sequence[i] = complementaryBase(complementary_Sequence[i]);
  }

  return complementary_Sequence;
}
char *complementarySequence(DNAArray dna)
{

  char *complementary_Sequence = new char[dna.size];
  int i, j;
  for (i = 0, j = dna.size - 1; (i < dna.size && j > 0); i++, j--)
  {
    complementary_Sequence[i] = dna.base[j];

    complementary_Sequence[i] = complementaryBase(complementary_Sequence[i]);
  }

  return complementary_Sequence;
}
char *analyzeDNA(char *base, int size, int &countA, int &countC, int &countG, int &countT)
{
  countA = arrays::countCharacter(&base[0], size, 'A');
  countC = arrays::countCharacter(&base[0], size, 'C');
  countG = arrays::countCharacter(&base[0], size, 'G');
  countT = arrays::countCharacter(&base[0], size, 'T');

  return complementarySequence(&base[0], size);
}
char *analyzeDNA(DNAArray dna, Counters &counters)
{
  counters.counterA = arrays::countCharacter(dna, 'A');
  counters.counterC = arrays::countCharacter(dna, 'C');
  counters.counterG = arrays::countCharacter(dna, 'G');
  counters.counterT = arrays::countCharacter(dna, 'T');

  return complementarySequence(dna);
}
}

#endif // DNA_HPP
